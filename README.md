##RNRPBot##

RNRPBot is a bot created for the RNRP Discord. This bot is a personal bot using JDA and is mostly a learning experience for me. 
***
### Planned Features ###
***
* Experience System
	1. Each time a person sends a message they'll be awarded some experience points. This will probably be about 5-10 experience points per message.
	2. There will be a cool down for every message sent so that someone cannot spam the chat and gain a bunch of exp.
	3. A Leaderboard will be present so we can see who has the most levels.
* Currency System
	1. You can gain sCoins by playing games on the server, you can also gain sCoin by logging in each day.
	2. Just like the Experience System, there will be a Leaderboard that displays who has the most sCoins.
	3. Ability to give sCoins to other people.
* TextToSpeech
	1. As a joke, we'd love to be able to TTS without using Discord's shitty TTS. 
	2. Also add a way for the bot to talk back to us.
		1. If we type something like "Tell Seaborn he sucks" then it'll hop in the Voice Channel and tell the person they are a terrible human being. *all jokes of course*
		2. This can be applied to just telling us random stuff as well. Perhaps some cat facts.

### To Do List ###
***
* Change the Help Command to display all different CommandCategories
* ~~Change the Bots Presence every 20 or so minutes~~ Completed
* Add more responses to AutoRespond
* Add these Commands
	1. ~~Shutdown - Shuts the bot down~~ Completed
	2. Restart - Restart the bot
	3. Image - Display a image from Google or Imgur depending on the <tags>
* ~~Be super sneaky, add an option to log any bot commands to a file. ~~ Completed
(Not only does it log commands, it logs basically everything. Anything typed into a textchannel)

### Dependencies ###
***
* [JDA](https://github.com/DV8FromTheWorld/JDA)
* [JDA-Utilities](https://github.com/JDA-Applications/JDA-Utilities)
* [JSoup](https://jsoup.org/)
* [Giphy4J](https://github.com/keshrath/Giphy4J)
* [SLF4J](https://www.slf4j.org/download.html)

### If you have any problems or suggestions, please tell me. ###