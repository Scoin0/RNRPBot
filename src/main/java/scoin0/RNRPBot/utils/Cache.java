package scoin0.RNRPBot.utils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;

/**
*
* @author John Grosh (john.a.grosh@gmail.com)
* 
*/

public class Cache {
	
	private final static int SIZE = 1000;
	private final HashMap<Long,FixedCache<Long,Message>> cache = new HashMap<>();
	
	public Message putMessage(Message msg) {
		if(!cache.containsKey(msg.getGuild().getIdLong())) {
			cache.put(msg.getGuild().getIdLong(), new FixedCache<>(SIZE));
		}
		return cache.get(msg.getGuild().getIdLong()).put(msg.getIdLong(), msg);
	}
	
	public Message pullMessage(Guild guild, long messageID) {
		if(!cache.containsKey(guild.getIdLong())) {
			return null;
		}
		return cache.get(guild.getIdLong()).pull(messageID);
	}
	
	public List<Message> getMessages(Guild guild, Predicate<Message> predicate) {
		if(!cache.containsKey(guild.getIdLong())) {
			return Collections.EMPTY_LIST;
		}
		return cache.get(guild.getIdLong()).getValues().stream().filter(predicate).collect(Collectors.toList());
	}
}
