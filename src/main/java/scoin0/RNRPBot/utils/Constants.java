package scoin0.RNRPBot.utils;

import java.awt.Color;

import net.dv8tion.jda.core.EmbedBuilder;

public class Constants {
	
	public static final String BitBucket = "https://bitbucket.org/Scoin0/rnrpbot/";
	
	public EmbedBuilder getNormalBuilder() {
	    return new EmbedBuilder().setColor(new Color(137, 38, 245));
	}
	
	public EmbedBuilder getErrorBuilder() {
	    return new EmbedBuilder().setColor(new Color(255, 43, 43));
	}
}
