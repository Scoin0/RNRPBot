package scoin0.RNRPBot.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import net.dv8tion.jda.core.entities.Game;
import scoin0.RNRPBot.RNRPBot;
import scoin0.RNRPBot.logger.RNRPLogger;

public class StatusChanger {
	
	private static List<String> status  = new ArrayList<String>();
	private static String temp;
	Timer timer;
	
	public StatusChanger() {
		File file = new File("Configuration/Status.txt");
		if(!file.exists()) {
			try {
				file.createNewFile();
				RNRPLogger.info("Please insert some status into the Status.txt!");
				RNRPBot.getJDA().shutdown();
			} catch (IOException e) {
				RNRPLogger.warn("Could not create Status.txt!");
				RNRPLogger.warn(e.getMessage());
			}
		} else {
			this.timer = new Timer();
			this.timer.schedule(new StatusChangerScheduled(), 0L, TimeUnit.HOURS.toMillis(5L));
		}
	}
	
	class StatusChangerScheduled extends TimerTask {
		
		StatusChangerScheduled() {}

		public void run() {
			try {
				Scanner readFile = new Scanner(new File("Configuration/Status.txt"));
				while(readFile.hasNext()) {
					StatusChanger.temp = readFile.nextLine();
					StatusChanger.status.add(StatusChanger.temp);
				}
				
				readFile.close();
				
				String[] tempStatus = (String[]) StatusChanger.status.toArray(new String[0]);
				Random random = new Random();
				int i = random.nextInt(tempStatus.length);
				RNRPBot.getJDA().getPresence().setGame(Game.playing((String) StatusChanger.status.get(i)));
				RNRPBot.getGuildLogChannel().sendMessage("Presence Changed: " + (String) StatusChanger.status.get(i)).queue();
				RNRPLogger.info("Presence Changed: " + (String) StatusChanger.status.get(i));
				StatusChanger.status.clear();
			} catch (IOException e) {
				RNRPLogger.severe("Could not read Status file!");
				RNRPLogger.severe(e.getMessage());
			}
		}
	}
}
