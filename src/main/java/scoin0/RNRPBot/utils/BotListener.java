package scoin0.RNRPBot.utils;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import scoin0.RNRPBot.RNRPBot;
import scoin0.RNRPBot.logger.RNRPLogger;

public class BotListener implements EventListener {

	@Override
	public void onEvent(Event event) {
		
		if(event instanceof GuildMessageReceivedEvent) {
			Message msg = ((GuildMessageReceivedEvent) event).getMessage();
			
			StringBuilder sb = new StringBuilder();
			sb.append("[Message] [" + msg.getTextChannel().getAsMention() + "] ") // [Message] [#Channel Name]
			.append(msg.getAuthor().getAsMention() + ": " + msg.getContentDisplay()); // @Name: Message
			
			if(!msg.getAuthor().isBot() && !msg.getMember().isOwner()) {
				RNRPLogger.info(sb.toString());
				RNRPBot.getGuildLogChannel().sendMessage(sb.toString()).queue();
			}
			
			RNRPBot.getMessageCache().putMessage(msg); //Still log my message but don't display them.
			
		} else if(event instanceof GuildMessageUpdateEvent) {
			Message msg = ((GuildMessageUpdateEvent) event).getMessage();
			Message old = RNRPBot.getMessageCache().putMessage(msg); //Still log my message but don't display them.
			
			if(!msg.getAuthor().isBot() && !msg.getMember().isOwner()) {
				
				StringBuilder sb = new StringBuilder();
				sb.append("[Message Edit] [" + msg.getTextChannel().getAsMention() + "] " + msg.getAuthor().getAsMention()) //[Message Edit] [#Channel Name]
				.append(" changed '" + old.getContentDisplay() + "'") // old Message
				.append(" to '" + msg.getContentDisplay() + "'"); // to new message
				
				RNRPLogger.info(sb.toString());
				RNRPBot.getGuildLogChannel().sendMessage(sb.toString()).queue();
			}
		} else if(event instanceof GuildMessageDeleteEvent) {
			GuildMessageDeleteEvent del = (GuildMessageDeleteEvent) event;
			Message old = RNRPBot.getMessageCache().pullMessage(del.getGuild(), del.getMessageIdLong());
			
			if(old.getMember().isOwner()) { //Still log my message but don't display them.
				return;
			}
			
			StringBuilder sb = new StringBuilder();
			sb.append("[Message Delete] [" + old.getTextChannel().getAsMention() + "] ")
			.append(old.getAuthor().getAsMention() + ": " + old.getContentDisplay());
			RNRPLogger.info(sb.toString());
			RNRPBot.getGuildLogChannel().sendMessage(sb.toString()).queue();
			
		}
		
	}

}
