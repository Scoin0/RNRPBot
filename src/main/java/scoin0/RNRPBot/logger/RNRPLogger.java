package scoin0.RNRPBot.logger;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RNRPLogger {
	
	private static Logger logger = Logger.getLogger("RNRPBot");
	
	public static void initialize() {
		
		try {
			logger.addHandler(new FileHandler(new File("logs", "log-" + getCurrentTimeStamp() + ".log").getPath()));
	    } catch (SecurityException e) {
	    	logger.severe(e.getMessage());
	    } catch (IOException e) {
	    	logger.severe(e.getMessage());
	    }
	}
	
	public static void info(String message) {
		logger.info(message + "\n");
	}
	  
	public static void warn(String message) {
	    logger.warning(message + "\n");
	}
	
	public static void severe(String message) {
		logger.severe(message + "\n");
	}
	  
	public static void debug(String message) {
	    logger.log(Level.OFF, "[DEBUG]" + message + "\n");
	}
	  
	public static void blankLine() {
	    System.out.println();
	}
	  
	public static String getCurrentTimeStamp() {
		SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
	    Date now = new Date();
	    return date.format(now);
	}
	  
	public static void init() {
	    File file = new File("Logs/");
	    if (!file.exists()) {
	      file.mkdirs();
	    } else {
	      initialize();
	   }
	}

}
