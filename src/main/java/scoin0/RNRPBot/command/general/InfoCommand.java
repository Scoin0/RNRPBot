package scoin0.RNRPBot.command.general;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class InfoCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	
	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		Guild guild = msg.getMember().getGuild();
		
		eb.setAuthor("Information", null, guild.getIconUrl());
		eb.setDescription("Information for " + guild.getName());
		eb.setThumbnail(guild.getIconUrl());
		eb.addField("Users (Online/Total)", (int) guild.getMembers().stream().filter(u -> !u.getOnlineStatus().equals(OnlineStatus.OFFLINE)).count() + "/" + guild.getMembers().size(), true);
		eb.addField("Date Created: ", guild.getCreationTime().format(DateTimeFormatter.ISO_DATE_TIME).replaceAll("[^0-9.:-]", " "), true);
		eb.addField("Owner: ", guild.getOwner().getUser().getName() + "#" + guild.getOwner().getUser().getDiscriminator(), true);
		eb.addField("View Source Code:", Constants.BitBucket, true);
		eb.setFooter("Server ID: " + String.valueOf(guild.getId()), null);
		
		msg.getTextChannel().sendMessage(eb.build()).queue();
	}

	@Override
	public String getName() {
		return "Bot Info";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!info");
	}

	@Override
	public String getDescription() {
		return "Pulls some information about the bot";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!info - " + getDescription());
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.GENERAL;
	}

}
