package scoin0.RNRPBot.command.general;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class HelpCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	private TreeMap<String, Command> commands;
	  
	public HelpCommand() {
	    this.commands = new TreeMap<String, Command>();
	}
	  
	public Command registerCommand(Command command) {
	    this.commands.put((String)command.getAliases().get(0), command);
	    return command;
	}
	  
	public void onCommand(MessageReceivedEvent msg, String[] args) {
	    if (!msg.isFromType(ChannelType.PRIVATE)) {
	    	this.eb.setTitle("Help Command:");
	    	this.eb.setColor(new Color(98, 247, 121));
	    	this.eb.setDescription(":mailbox_with_mail: " + msg.getAuthor().getAsMention() + ", Helpful information was sent to your inbox!\n" + 
	    			"Soon this will be changed for the introduction of Command Categories!");
	    	msg.getTextChannel().sendMessage(this.eb.build()).queue();
	    }
	    sendPrivate((PrivateChannel)msg.getAuthor().openPrivateChannel().complete(), args);
	}
	  
	public String getName() {
		return "Help";
	}
	  
	public List<String> getAliases() {
	    return Arrays.asList(new String[] { "!help", "!commands" });
	}
	  
	public String getDescription() {
	    return "Displays a list of Commands";
	}
	  
	public List<String> getUsage() {
	    return Collections.singletonList("!help or !help <command>\n!help - returns a list of commands\n!help <command> - returns the usage for the command \nExample, !help rtd");
	}
	  
	private void sendPrivate(PrivateChannel channel, String[] args) {
	    this.eb.setTitle("Help Command:");
	    this.eb.setColor(new Color(98, 247, 121));
	    if (args.length < 2) {
	    	StringBuilder sB = new StringBuilder();
	    	
	    	for (Command c : this.commands.values()) {
	    		String description = c.getDescription();
	    		description = (description == null) || (description.isEmpty()) ? "There's no description for this command!" : description;
	        
	    		sB.append("**").append((String)c.getAliases().get(0)).append("** - ");
	    		sB.append(description).append("\n");
	    	}
	      channel.sendMessage(new EmbedBuilder().setTitle("Help Command:").setColor(new Color(98, 247, 121)).setDescription(sB.toString()).build()).queue();
	    } else {
	    	String command = '!' + args[1];
	    	for (Command c : this.commands.values()) {
	    		if (c.getAliases().contains(command)) {
	    			String name = c.getName();
	    			String description = c.getDescription();
	    			List<String> usageInstructions = c.getUsage();
	    			name = (name == null) || (name.isEmpty()) ? "There's no name for this command!" : name;
	    			description = (description == null) || (description.isEmpty()) ? "There's no description for this command!" : description;
	    			usageInstructions = (usageInstructions == null) || (usageInstructions.isEmpty()) ? Collections.singletonList("There's no display usage for this command!") : usageInstructions;
	          
	    			StringBuilder hB = new StringBuilder();
	          
	    			hB.append("**Name:** " + name + "\n")
	    			.append("**Description:** " + description + "\n")
	    			.append("**Alliases:** " + String.join(", ", c.getAliases()) + "\n")
	    			.append("**Usage:** ")
	    			.append((String)usageInstructions.get(0));
	          
	    			channel.sendMessage(new EmbedBuilder().setTitle("Usage for " + args[1]).setColor(new Color(98, 247, 121)).setDescription(hB.toString()).build()).queue();
	    			for (int i = 1; i < usageInstructions.size(); i++) {
	    				channel.sendMessage(new MessageBuilder().append("__" + name + " Usage Cont. (" + (i + 1) + ")__\n").append((CharSequence)usageInstructions.get(i)).build()).queue();
	    			}
	    			return;
	    		}
	    	}
	    	channel.sendMessage(new MessageBuilder().append("The provided command '**" + args[1] + "**" + " does not exist. Use !help to get a list of commands.").build()).queue();
	    }
	}
	  
	public CommandCategory commandCategory() {
	    return CommandCategory.GENERAL;
	}
	
}