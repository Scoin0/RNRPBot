package scoin0.RNRPBot.command.general;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import at.mukprojects.giphy4j.Giphy;
import at.mukprojects.giphy4j.entity.giphy.GiphyData;
import at.mukprojects.giphy4j.entity.search.SearchFeed;
import at.mukprojects.giphy4j.exception.GiphyException;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.config.BotConfig;
import scoin0.RNRPBot.logger.RNRPLogger;
import scoin0.RNRPBot.utils.Constants;

public class GifSearchCommand extends Command {
	
	BotConfig config = new BotConfig();

	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
	    EmbedBuilder eb = new Constants().getNormalBuilder();
	    
	    if (args.length < 2) {
	    	sendMessage(msg, "Please insert some searchable tags");
	    } else {
	    	String[] query = msg.getMessage().getContentRaw().split(" ", 2);
	    	String search = query[1].replace(" ", "+");
	    	
	    	try {
	    		Giphy gif = new Giphy(config.getGiphyToken());
	    		SearchFeed feed = gif.search(search, 0);
	    		Random ran = new Random();
	    		int gifindex = ran.nextInt(feed.getDataList().size());
	    		eb.setTitle("Here's your gif, " + msg.getMember().getEffectiveName());
	    		eb.setColor(new Color(98, 247, 121));
	    		eb.setImage(((GiphyData)feed.getDataList().get(gifindex)).getImages().getOriginal().getUrl());
	    		msg.getChannel().sendMessage(eb.build()).queue();
	    	} catch (GiphyException e) {
	    		RNRPLogger.warn("Couldn't not look up the gif!");
	    		RNRPLogger.warn(e.getMessage());
	    	}
	    }
	}

	public String getName() {
		return "Gif Search";
	}

	public List<String> getAliases() {
		return Arrays.asList(new String[] { "!gif" });
	}

	public String getDescription() {
		return "Searches for a gif on giphy";
	}

	public List<String> getUsage() {
		return Collections.singletonList("!gif <tags>");
	}

	public CommandCategory commandCategory() {
		return CommandCategory.GENERAL;
	}
}
