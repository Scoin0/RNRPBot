package scoin0.RNRPBot.command.general;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class OsuCommand extends Command{

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		EmbedBuilder eb = new Constants().getNormalBuilder();
		EmbedBuilder er = new Constants().getErrorBuilder();
		
		if(args.length == 1) {
			er.setTitle("Wrong use of Command:");
			er.setDescription("Please insert a name of the user you're looking for.");
			msg.getChannel().sendMessage(er.build()).queue();
		} else if (args[1] != null) {
			eb.setTitle("Osu Profile Lookup");
			eb.setImage("https://lemmmy.pw/osusig/sig.php?colour=pink&uname=" + args[1] + "&pp=2&flagshadow&flagstroke&darktriangles&opaqueavatar&onlineindicator=undefined&xpbar&xpbarhex");
			msg.getChannel().sendMessage(eb.build()).queue();
		}
		
	}

	@Override
	public String getName() {
		return "Osu";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!osu");
	}

	@Override
	public String getDescription() {
		return "Return a nice profile image of the person you're looking up";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!osu <name> - Returns a profile image of the person you're looking for");
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.GENERAL;
	}
}
