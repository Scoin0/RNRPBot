package scoin0.RNRPBot.command.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.MessageBuilder;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.RNRPBot;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class BroomCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();
	ArrayList<String> list = new ArrayList<String>();

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		VoiceChannel channel = RNRPBot.getJDA().getVoiceChannelById("327680380654190593");
		
		if(msg.getMember().getUser().isBot()) {
			return;
		}
		
		if(args.length > 1  && !list.contains(msg.getAuthor().getAvatarId())) {
			eb.setTitle("YOU'VE BEEN BROOMED!");
			eb.setImage("https://i.imgur.com/dUSaCPp.png");
			msg.getMessage().getMentionedMembers().stream().forEach(m -> msg.getGuild().getController().moveVoiceMember(m, channel).queue());
			msg.getChannel().sendMessage(eb.build()).queue();
			msg.getChannel().deleteMessageById(msg.getMessage().getId()).complete();
			list.add(msg.getAuthor().getAvatarId());
			time(list, 60 * 1000, msg.getAuthor().getAvatarId());
		} else if(args.length == 1) {
			er.setTitle("Wrong use of command!");
			er.setDescription("Please enter the name of the person you are trying to broom!\n" + 
							  "Like so, !broom @<name>");
			msg.getChannel().sendMessage(er.build()).queue();
		} else {
			er.setTitle("You are on cool down...");
			er.setDescription("I'm sorry " + msg.getAuthor().getAsMention() + " you are currently on a cool down.\n");
			msg.getChannel().sendMessage(er.build()).queue();
		}
		
	}
	
	public void time(final ArrayList<String> list, final int t, final String s){
		new Thread(){
			public void run(){
				try{
					Thread.sleep(t);
					list.remove(s);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}.start();
	}

	@Override
	public String getName() {
		return "Broom";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!broom", "!kick");
	}

	@Override
	public String getDescription() {
		return "Broom a user";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!kick <user> - Kick a user to the broomed channel" +
										 "!broom <user> - Broom a user to the broomed channel");
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.ADMIN;
	}
}
