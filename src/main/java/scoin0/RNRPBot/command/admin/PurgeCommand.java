package scoin0.RNRPBot.command.admin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.exceptions.PermissionException;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class PurgeCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();
	
	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		TextChannel channel = msg.getTextChannel();
		
		if(args.length == 1) {
			er.setTitle("Wrong use of command:");
			er.setDescription("You must have a number so that I might know how much to purge.");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else if (!msg.getMember().isOwner()){
			er.setTitle("Error in Permissions:");
			er.setDescription("I'm sorry, you are not Scoin0. You cannot use this command.");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else {
			Integer pmsg = 0;
			
			try {
				
				pmsg = Integer.parseInt(args[1]);
				System.out.println("Purging " + pmsg + " messages!");
				
			} catch (NumberFormatException e) {
				er.setTitle("Error:");
				er.setDescription("Please enter a vaild number.");
				msg.getTextChannel().sendMessage(er.build()).queue();
			}
			
			if(pmsg <= 1 || pmsg > 100) {
				er.setTitle("Error:");
				er.setDescription("Please enter a number between **2 and 100**.");
				msg.getTextChannel().sendMessage(er.build()).queue();
				return;
			}
			
			msg.getTextChannel().deleteMessageById(msg.getMessage().getId()).complete();
			channel.getHistory().retrievePast(pmsg).queue((List<Message> mess) -> {
                try {
                    msg.getTextChannel().deleteMessages(mess).queue(
                            success ->
                                    channel.sendMessage(" `" + args[1] + "` messages deleted.")
                                           .queue(message -> {
                                                message.delete().queueAfter(2, TimeUnit.SECONDS);
                                            }),
                            error -> channel.sendMessage(" An Error occurred!").queue());
                } catch (IllegalArgumentException iae) {
                    msg.getChannel().sendMessage(" Cannot delete messages older than 2 weeks.").queue();
                } catch (PermissionException pe) {
                    throw pe;
                }
            });
			
		}
		
		
	}

	@Override
	public String getName() {
		return "Purge";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!purge");
	}

	@Override
	public String getDescription() {
		return "Purge this discord of filth!";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!purge <number> - Delete the selected amount of messages in the channel.");
	}
	
	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.ADMIN;
	}
	
}