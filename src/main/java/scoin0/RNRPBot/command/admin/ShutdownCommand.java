package scoin0.RNRPBot.command.admin;

import java.util.Arrays;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.RNRPBot;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.logger.RNRPLogger;
import scoin0.RNRPBot.utils.Constants;

public class ShutdownCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		if (!msg.getMember().isOwner()) {
			er.setTitle("Error in Permissions:");
			er.setDescription("I'm sorry, you are not Scoin0. You cannot use this command.");
			msg.getTextChannel().sendMessage(this.er.build()).queue();
	    } else {
	    	RNRPBot.getGuildLogChannel().sendMessage("Shutdown issued. Goodbye :frowning:").complete();
	    	RNRPLogger.warn("Shutdown issued...");
	    	RNRPBot.getJDA().shutdown();
	    	System.exit(0);
	    }
	}

	@Override
	public String getName() {
		return "Shutdown";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!shutdown", "!sd" );
	}

	@Override
	public String getDescription() {
		return "Force shuts the bot down";
	}

	@Override
	public List<String> getUsage() {
		return null;
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.ADMIN;
	}

}
