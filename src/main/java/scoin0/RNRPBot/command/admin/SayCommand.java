package scoin0.RNRPBot.command.admin;

import java.awt.Color;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class SayCommand extends Command {

	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		if(args.length == 1) { 
			er.setTitle("Wrong use of Command:");
			er.setDescription("I need a message in order to say something");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else if(!msg.getMember().isOwner()) {
			er.setTitle("Error in Permissions:");
			er.setDescription("I'm sorry, you are not Scoin0. You cannot use this command.");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else {
			msg.getChannel().deleteMessageById(msg.getMessage().getId()).complete();
			String[] getMessage = msg.getMessage().getContentRaw().split(" ", 2);
			String setMessage = getMessage[1].replace(" ", " ");
			 
			eb.setColor(new Color(242, 118, 240));
			eb.setDescription(setMessage);
			msg.getChannel().sendMessage(eb.build()).queue();
		}
	}

	@Override
	public String getName() {
		return "Say";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!say", "!s");
	}

	@Override
	public String getDescription() {
		return "Become like the bot! Say words!";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!say <message> - tells the bot to say something.");
	}
	
	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.ADMIN;
	}
}