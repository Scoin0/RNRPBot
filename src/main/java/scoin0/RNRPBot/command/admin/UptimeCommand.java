package scoin0.RNRPBot.command.admin;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Arrays;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class UptimeCommand extends Command {
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		RuntimeMXBean rb = ManagementFactory.getRuntimeMXBean();;
		
	    long time = rb.getUptime();
	    int seconds = (int)(time / 1000L) % 60;
	    int minutes = (int)(time / 60000L % 60L);
	    int hours = (int)(time / 3600000L % 24L);
	    int days = (int)(time / 86400000L);

		eb.setTitle(getName());
		eb.setDescription("The bot has been running for " + days + " days, " + hours + " hours, " + minutes + " minutes and " + seconds + " seconds.");
		msg.getTextChannel().sendMessage(eb.build()).queue();
		
	}

	@Override
	public String getName() {
		return "Uptime";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!uptime");
	}

	@Override
	public String getDescription() {
		return "Returns how long the Bot has been running for.";
	}

	@Override
	public List<String> getUsage() {
		return null;
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.ADMIN;
	}
}
