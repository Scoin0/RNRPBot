package scoin0.RNRPBot.command.currency;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import scoin0.RNRPBot.command.Command;
import scoin0.RNRPBot.command.CommandCategory;
import scoin0.RNRPBot.utils.Constants;

public class FlipCommand extends Command{
	
	EmbedBuilder eb = new Constants().getNormalBuilder();
	EmbedBuilder er = new Constants().getErrorBuilder();
	SecureRandom random = new SecureRandom();
	
	public String heads = "https://www.clker.com/cliparts/7/d/e/0/139362185558690588heads-md.png";
	public String tails = "https://upload.wikimedia.org/wikipedia/commons/5/5a/98_quarter_reverse.png";

	@Override
	public void onCommand(MessageReceivedEvent msg, String[] args) {
		
		if(args.length ==  1) {
			er.setTitle("Wrong use of Command:");
			er.setDescription("I need both a number of monies and a selection of either heads or tails!");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else if(args.length == 2) {
			er.setTitle("Wrong use of Command:");
			er.setDescription("I need a choice of heads or tails!");
			msg.getTextChannel().sendMessage(er.build()).queue();
		} else if (args.length == 3){
			int i = Integer.parseInt(args[1]);
			boolean d = random.nextBoolean();
			eb.setTitle("Flip Command");
			if(d == true && args[2].equalsIgnoreCase("h")) {
				eb.setImage(heads);
				int k = i * 2;
				eb.setDescription("You're right! You've gained " + k + " monies. ");
				msg.getChannel().sendMessage(eb.build()).queue();
			} else if(d == false && args[2].equalsIgnoreCase("t")) {
				eb.setImage(tails);
				int k = i * 2;
				eb.setDescription("You're right! You've gained " + k + " monies. ");
				msg.getChannel().sendMessage(eb.build()).queue();
			} else {
				er.setImage(d ? heads : tails);
				int k = i * 2;
				er.setDescription("You're wrong! You've lost " + k + " monies. ");
				msg.getChannel().sendMessage(er.build()).queue();
			}
		}
	}

	@Override
	public String getName() {
		return "Flip";
	}

	@Override
	public List<String> getAliases() {
		return Arrays.asList("!flip");
	}

	@Override
	public String getDescription() {
		return "Flip a coin to win monies";
	}

	@Override
	public List<String> getUsage() {
		return Collections.singletonList("!flip <num> <h/t> - Flip a coin and choose if it's gonna be heads or tails.");
	}

	@Override
	public CommandCategory commandCategory() {
		return CommandCategory.CURRENCY;
	}
}
