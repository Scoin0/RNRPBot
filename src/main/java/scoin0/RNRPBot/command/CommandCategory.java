package scoin0.RNRPBot.command;

public enum CommandCategory {
	
	ADMIN("admin", ":OK_HAND:", "Admin"),  
	GENERAL("general", ":OK_HAND:", "General"),  
	FUN("fun", ":OK_HAND:", "Fun"),  CURRENCY("currency", ":OK_HAND:", "Currency"),  
	EXPERIENCE("experience", ":OK_HAND:", "Experience"),  
	UNKNOWN("nopackage", ":OK_HAND:", "MISC");
	  
	private final String category;
	private final String emoticon;
	private final String displayName;
	  
	private CommandCategory(String category, String emoticon, String displayName) {
	    this.category = category;
	    this.emoticon = emoticon;
	    this.displayName = displayName;
	}
	  
	public static CommandCategory fromPackage(String category) {
		
	    if (category != null) {
	    	CommandCategory[] arrayOfCommandCategory;
	    	int j = (arrayOfCommandCategory = values()).length;
	    	for (int i = 0; i < j; i++) {
	    		CommandCategory cc = arrayOfCommandCategory[i];
	    		if (category.equalsIgnoreCase(cc.category)) {
	    			return cc;
	    		}
	    	}
	    }
	    return UNKNOWN;
	}
	  
	public String getDisplayName() {
	    return this.displayName;
	}
	  
	public String getCategory() {
	    return this.category;
	}
	  
	public String getEmoticon() {
	    return this.emoticon;
	}
	
}
