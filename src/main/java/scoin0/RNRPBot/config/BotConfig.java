package scoin0.RNRPBot.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scoin0.RNRPBot.logger.RNRPLogger;

public class BotConfig {
	
	public static final Logger log = LoggerFactory.getLogger("BotConfig");
	private static String botToken = "token";
	private static String botName = "RNRPBot";
	private static String botChannel = "channel id";
	private static String prefix = "!";
	private static String DB_USER = "RNRPBot";
	private static String DB_PASS = "rnrpBot";
	private static String DB_NAME = "RNRPBot";
	private static String DB_HOST = "107.180.12.114";
	private static String DB_PORT = "3306";
	private static String giphyToken = "";
	private static String osuToken = "";
	private static String ownerID = "";
	private static String guildID = "";
	  
	public void createProps() {
		Properties props = new Properties();
		try {
			 props.setProperty("bot-token", botToken);
			 props.setProperty("bot-name", botName);
			 props.setProperty("bot-channel", botChannel);
			 props.setProperty("bot-prefix", prefix);
	      
			 props.setProperty("db_user", DB_USER);
			 props.setProperty("db_pass", DB_PASS);
			 props.setProperty("db_name", DB_NAME);
			 props.setProperty("db_host", DB_HOST);
			 props.setProperty("db-port", DB_PORT);
	      
			 props.setProperty("giphy-token", giphyToken);
			 props.setProperty("osu-token", osuToken);
			 props.setProperty("owner-id", ownerID);
			 props.setProperty("guild-id", guildID);
	      
			 props.store(new FileOutputStream("Configuration/config.properties"), null);
	    } catch (Exception e) {
	      RNRPLogger.warn("Couldn't create Props file!");
	    }
	}
	
	public void loadProps() {
		
	    try {
	    	File config = new File("Configuration/config.properties");
	    	FileInputStream fileInput = new FileInputStream(config);
	    	Properties props = new Properties();
	    	props.load(fileInput);
	    	fileInput.close();
	      
	    	botToken = props.getProperty("bot-token");
	    	botName = props.getProperty("bot-name");
	    	botChannel = props.getProperty("bot-channel");
	    	prefix = props.getProperty("bot-prefix");
	      
	    	DB_NAME = props.getProperty("db_name");
	    	DB_USER = props.getProperty("db_user");
	    	DB_PASS = props.getProperty("db_pass");
	    	DB_HOST = props.getProperty("db_host");
	    	DB_PORT = props.getProperty("db_port");
	      
	    	giphyToken = props.getProperty("giphy-token");
	    	osuToken = props.getProperty("osu-token");
	    	ownerID = props.getProperty("owner-id");
	    	guildID = props.getProperty("guild-id");
	    } catch (Exception e) {
	    	RNRPLogger.warn("Couldn't find file!");
	    	e.printStackTrace();
	    }
	}

	public String getBotToken() {
		return botToken;
	}

	public String getBotName() {
		return botName;
	}

	public String getBotChannel() {
		return botChannel;
	}

	public String getPrefix() {
		return prefix;
	}

	public String getDB_USER() {
		return DB_USER;
	}

	public String getDB_PASS() {
		return DB_PASS;
	}

	public String getDB_NAME() {
		return DB_NAME;
	}

	public String getDB_HOST() {
		return DB_HOST;
	}

	public String getDB_PORT() {
		return DB_PORT;
	}
	
	public String getGiphyToken() {
		return giphyToken;
	}
	
	public String getOsuToken() {
		return osuToken;
	}

	public String getOwnerID() {
		return ownerID;
	}

	public String getGuildID() {
		return guildID;
	}
}
