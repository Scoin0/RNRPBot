package scoin0.RNRPBot;

import java.io.File;

import javax.security.auth.login.LoginException;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import scoin0.RNRPBot.command.admin.BroomCommand;
import scoin0.RNRPBot.command.admin.PurgeCommand;
import scoin0.RNRPBot.command.admin.SayCommand;
import scoin0.RNRPBot.command.admin.ShutdownCommand;
import scoin0.RNRPBot.command.admin.UptimeCommand;
import scoin0.RNRPBot.command.currency.FlipCommand;
import scoin0.RNRPBot.command.general.GifSearchCommand;
import scoin0.RNRPBot.command.general.HelpCommand;
import scoin0.RNRPBot.command.general.InfoCommand;
import scoin0.RNRPBot.command.general.OsuCommand;
import scoin0.RNRPBot.config.BotConfig;
import scoin0.RNRPBot.logger.RNRPLogger;
import scoin0.RNRPBot.utils.BotListener;
import scoin0.RNRPBot.utils.Cache;
import scoin0.RNRPBot.utils.StatusChanger;

public class RNRPBot {
	
	private static RNRPBot instance;
	private static JDA jda;
	private static Cache messages;
	private static BotConfig config = new BotConfig();
	private static String version = "2.0.5";
	
	public static void main(String[] args) {
		System.out.println(getLogo());
		
		RNRPLogger.init();
		RNRPLogger.blankLine();
		RNRPLogger.info("Starting bot...");
		messages = new Cache();
		loadProps();
		
		try {
			jda = new JDABuilder(AccountType.BOT).setToken(config.getBotToken()).addEventListener(new BotListener()).buildBlocking();
			RNRPLogger.info("Adding commands to bot...");
			addCommands();
		} catch (LoginException e) {
			RNRPLogger.severe("There was a problem logging in!");
			RNRPLogger.severe(e.getMessage());
		} catch (InterruptedException e) {
			RNRPLogger.severe("The connection was interrupted! Is this bot already running?");
			RNRPLogger.severe(e.getMessage());
		}
		new StatusChanger();
		RNRPLogger.info("RNRPBot Version " + version + " is online!");
	}
	
	public static void addCommands() {
		HelpCommand help = new HelpCommand();
		
		jda.addEventListener(help.registerCommand(help));
		
		if(config.getGiphyToken() != null && (!config.getGiphyToken().isEmpty())) {
			jda.addEventListener(help.registerCommand(new GifSearchCommand()));
		} else {
			RNRPLogger.warn("Giphy API key isn't provided. Gif Searching has been disabled!");
			getGuildLogChannel().sendMessage("Giphy API key isn't provided. Gif Searching has been disabled!").complete();
		}
		
		//ADMIN COMMANDS
		jda.addEventListener(help.registerCommand(new BroomCommand()));
		jda.addEventListener(help.registerCommand(new PurgeCommand()));
		jda.addEventListener(help.registerCommand(new UptimeCommand()));
		jda.addEventListener(help.registerCommand(new SayCommand()));
		jda.addEventListener(help.registerCommand(new ShutdownCommand()));
		
		//CURRENCY COMMANDS
		jda.addEventListener(help.registerCommand(new FlipCommand()));
		
		//GENERAL COMMANDS
		jda.addEventListener(help.registerCommand(new OsuCommand()));
		jda.addEventListener(help.registerCommand(new InfoCommand()));
	}
	
	public static void loadProps() {
		File file = new File("Configuration/config.properties");
		if(!file.exists()) {
			config.createProps();
		} else {
			config.loadProps();
			RNRPLogger.info("Loaded Configuration File");
		}
	}
	
	public static Cache getMessageCache() {
		return messages;
	}
	
	public static TextChannel getGuildLogChannel() {
		return getJDA().getGuildById(config.getGuildID()).getTextChannelById(config.getBotChannel());
	}
	
	public static JDA getJDA() {
		return jda;
	}
	
	public static RNRPBot getInstance() {
		return instance;
	}
	
	
	public static String getLogo() {
		//This looks super scuffed ;)
	    return "______ _   _ __________________       _   \r\n" +
	    		"| ___ \\ \\ | || ___ \\ ___ \\ ___ \\     | |  \r\n" +
	    		"| |_/ /  \\| || |_/ / |_/ / |_/ / ___ | |_ \r\n" +
	    		"|    /| . ` ||    /|  __/| ___ \\/ _ \\| __|\r\n" +
	    		"| |\\ \\| |\\  || |\\ \\| |   | |_/ / (_) | |_ \r\n" +
	    		"\\_| \\_\\_| \\_/\\_| \\_\\_|   \\____/ \\___/ \\__|";
	}

}
